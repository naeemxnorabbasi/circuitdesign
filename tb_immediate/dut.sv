module dut(clk,reset,a,b);
  
  input logic clk, reset, a, b;
  
  // property declaration
  property mutex (a, b);
    (!(a & b));
  endproperty

  // immediate assertion
  asrt_mutex: assert property (mutex(a, b))  else 
    $error("a = %0d; b = %0d; at: %0t", a, b, $time);
  
  // cover property
  cov_mutex: cover property (.a(a), .b(b));

endmodule

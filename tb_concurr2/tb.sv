module tb;
   // parameters
   parameter PERIOD = 10;
   parameter HALF_PERIOD = PERIOD/2;
   parameter SIM_TIME = 120;

   // signal declaration
   logic reset, clk;
   logic a,b;

  // generate clock
   always begin
      clk <= 0;
      #HALF_PERIOD;
      clk <= 1;
      #HALF_PERIOD;
   end

   // dump waves and end simulation
   initial begin
      $dumpfile("waves.vcd");
      $dumpvars;
      #SIM_TIME;
     $finish();
   end

   //--------------------------------------------
   // TODO: generate stimulus - update as needed
   //--------------------------------------------
   initial begin
      clk = 0;
      reset = 1;
      a = 0; 
      b = 0;
      #1;
     reset = 0;
     #14; 
     a = 1;
     #10;
     a = 0;
     b = 1;
     #10;
     b = 0;
     #20;
     a = 1;
     #10;
     a = 0;
     #20;
     a = 1;
     #10;
     a = 0;
     b = 1;
     #10;
     b = 0;           
   end // initial begin

//------------------------------------   
// TODO: insert your assertions here
//------------------------------------   
   
  input logic clk, reset, a, b;
  
  // property declaration
  property if_a_then_b (clock, reset, a, b);
    @(posedge clock) disable iff (reset) ((a) |=> (b));
  endproperty

  // concurrent assertion
  asrt_if_a_then_b: assert property (if_a_then_b(clk, reset, a, b))  else 
    $error("a = %0d; b = %0d; at: %0t", $past(a), b, $time);
  
  // cover property
  cov_if_a_then_b: cover property (if_a_then_b(.clock(clk), .reset(reset), .a(a), .b(b)));

  
endmodule // tb

/*----------------------------------------------------
The bind statement syntax is as follows:

bind dut_module ast_module ast_inst (signal mappings);

Here is a simple example:

Lets suppose we have a module called ask_blk which contains property declaration and specifications.

We can bind this module the DUT in the test bench as follows:

bind top ast_blk ast_blk_inst (c_clk, a_top, b_top);

Here:
bind - language key or reserved word.
top is the DUT to which we would connect the assertion module ast_blk. ast_blk_inst is the instance of the assertions module ast_blk in the test bench. clk, atop, and btop signals are passed to the assertion module instance from the test bench.
----------------------------------------------------*/

// test bench that instantiates the DUT and then binds the assertion module to it
module tb;
   
   parameter PERIOD = 10;
   parameter HALF_PERIOD = PERIOD/2;
   parameter SIM_TIME = 120;
   
   logic reset, clk;
   logic a,b;

   initial begin
      clk = 0;
      reset = 1;
      a = 0; 
      b = 0;
      #1;
     reset = 0;
     #14; 
     a = 1;
     #10;
     a = 0;
     b = 1;
     #10;
     b = 0;
     #20;
     a = 1;
     #10;
     a = 0;
     #20;
     a = 1;
     #10;
     a = 0;
     b = 1;
     #10;
     b = 0;     
   end  
  
   always begin
      clk <= 0;
      #HALF_PERIOD;
      clk <= 1;
      #HALF_PERIOD;
   end

   initial begin
      $dumpfile("waves.vcd");
      $dumpvars;
      #SIM_TIME;
     $finish();
   end

  
// instantiate DUT
top top_inst (.clk(clk), .reset(reset), .a(a) , .b(b), .f(f));

// bind the Assertions module to DUT
bind top ast_blk ast_clk_inst(.clk(clk), .reset(reset), .a(a), .b(b));

  
endmodule // tb

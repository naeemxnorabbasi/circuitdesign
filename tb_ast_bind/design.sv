// design and assertion modules
// DUT
module top(input logic clk, reset, a , b, output logic f);
  always @(posedge clk) begin
     if(reset) begin
       f <= 0;
     end else begin
       f <= a || b;
     end
  end
endmodule

// Assertions module
module ast_blk(input logic clk, a, b, reset);

property prop_a_implies_b(logic clk, reset, a, b);
  @(posedge clk) disable iff(reset) a |=> b;
endproperty

//  lbl_prop_a_implies_b: assert property(prop_a_implies_b(.clk(clk), .reset(reset), .a(a), .b(b))) $display("PASS: %0t", $time); else $display("FAIL: %0t", $time);

  lbl_prop_a_implies_b: assert property(prop_a_implies_b(.clk(clk), .reset(reset), .a(a), .b(b))) $info("PASS: %0t", $time); else $info("FAIL: %0t", $time);


  lbl_cov_prop_a_implies_b: cover property(prop_a_implies_b(.clk(clk), .reset(reset), .a(a), .b(b))) $info("PASS: %0t", $time); // else $info("FAIL: %0t", $time);
    
    
endmodule


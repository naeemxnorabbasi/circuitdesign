// hello world - $display system function
module tb;

  integer a = 0;
  
  initial begin
    a = 5;
    $display("a: %0d", a);
  end
  
endmodule // tb

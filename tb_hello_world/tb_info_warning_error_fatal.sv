// $info, $warning, $error, $fatal system functions
module tb;

  integer a = 0;
  
  initial begin
    a = 5;
    $info("a: %0d", a);
    a = 5;
    $warning("a: %0d", a);
    a = 5;
    $error("a: %0d", a);
    a = 5;
    $fatal("a: %0d", a);
  end
  
endmodule // tb

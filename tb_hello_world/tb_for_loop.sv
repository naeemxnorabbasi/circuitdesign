// for loop

module tb;

  integer i = 0;
  
  initial begin
     for(i=0;i<10;i++) begin
	$display("i: %0d", i);
     end
  end
  
endmodule // tb




// Code your testbench here
// or browse Examples


module and4_gate(x0,x1,x2,x3,f);
    input logic x0, x1,x2,x3;
    output logic f;

    assign f = x0 & x1 & x2 & x3;


endmodule


module nor4_gate(x0,x1,x2,x3,f);
    input logic x0, x1,x2,x3;
    output logic f;

    assign f = ~(x0 | x1 | x2 | x3);

endmodule

module inv_gate(a,f);
    input logic a;
    output logic f;

    assign f = ~a;

endmodule


module four_input_mux_aoi_logic(x0,x1,x2,x3,s0,s1,strobe_n,out_f,out_f_bar);
    input logic x0;
    input logic x1;
    input logic x2;
    input logic x3;
    input logic s0;
    input logic s1;
    input logic strobe_n;
    output logic out_f;
    output logic out_f_bar;

logic s0_bar, s1_bar, strobe_n_bar, s0_buf,s1_buf;
logic and4_0, and4_1, and4_2, and4_3;
logic nor4_0;

inv_gate u_inv_strobe(.a(strobe_n),.f(strobe_n_bar));

inv_gate u_inv_s0_0(.a(s0),.f(s0_bar));
inv_gate u_inv_s0_1(.a(s0_bar),.f(s0_buf));
inv_gate u_inv_s1_0(.a(s1),.f(s1_bar));
inv_gate u_inv_s1_1(.a(s1_bar),.f(s1_buf));

and4_gate u_and4_gate_0(.x0(s0_bar),.x1(s1_bar),.x2(x0),.x3(strobe_n_bar),.f(and4_0));
and4_gate u_and4_gate_1(.x0(s0_buf),.x1(s1_bar),.x2(x1),.x3(strobe_n_bar),.f(and4_1));
and4_gate u_and4_gate_2(.x0(s0_bar),.x1(s1_buf),.x2(x2),.x3(strobe_n_bar),.f(and4_2));
and4_gate u_and4_gate_3(.x0(s0_buf),.x1(s1_buf),.x2(x3),.x3(strobe_n_bar),.f(and4_3));

nor4_gate u_nor4_gate (.x0(and4_0),.x1(and4_1),.x2(and4_2),.x3(and4_3),.f(nor4_0));

assign out_f = nor4_0;
  
inv_gate u_inv_nor4(.a(nor4_0),.f(out_f_bar));

endmodule


module tb();

    logic x0 = 0;
    logic x1 = 0;
    logic x2 = 0;
    logic x3 = 0;
    logic s0 = 0;
    logic s1 = 0;
    logic strobe_n = 0;
    logic out_f;
    logic out_f_bar;

    four_input_mux_aoi_logic u_four_input_mux_aoi_logic(
        .x0(x0),
        .x1(x1),
        .x2(x2),
        .x3(x3),
        .s0(s0),
        .s1(s1),
      .strobe_n(strobe_n),
        .out_f(out_f),
        .out_f_bar(out_f_bar)
        );


    integer i = 0;
    integer j = 0;
    integer k = 0;
    integer l = 0;
    integer m = 0;
    integer n = 0;
    integer p = 0;

        initial begin

        repeat(3) begin
            for (i=0;i<2;i++) begin
                strobe_n = ~strobe_n; #1;
                for(j=0;j<2;j++) begin
                    s1 = ~s1; #1;
                    for(k=0;k<2;k++) begin
                        s0 = ~s0; #1;
                        for (l=0;l<2;l++) begin
                            x3 = ~x3; #1;
                            for(m=0;m<2;m++) begin
                                x2 = ~x2; #1;
                                for(n=0;n<2;n++) begin
                                    x1 = ~x1; #1;
                                    for(p=0; p<2;p++) begin
                                        x0 = ~x0; #1;
                                    end
                                end
                            end
                        end
                    end
                end
            end
            #1;
        end
    end


        initial begin
            $dumpvars;
            $dumpfile("waves.vcd");
        end


endmodule




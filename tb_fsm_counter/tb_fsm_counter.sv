module counter_fsm_tb;

    // Parameters
    parameter CLOCK_PERIOD = 10; // Clock period in ns
    parameter RESET_DURATION = 1; // Reset duration in ns
    parameter SIMULATION_TIME = 500; // Simulation time in ns
  
    // Inputs
    reg clk = 0;
    reg reset = 1;
  
    // Outputs
    wire [1:0] state;
  
    // Instantiate the counter FSM module
    counter_fsm dut (
      .clk(clk),
      .reset(reset),
      .state(state)
    );
  
    // Clock generation
    always begin
      #(CLOCK_PERIOD/2) clk = ~clk;
    end
  
    // Reset generation
    initial begin
      reset = 1;
      #RESET_DURATION;
      reset = 0;
    end
  
    // Dump waveform to VCD file
    initial begin
      $dumpfile("waves.vcd");
      $dumpvars(0, counter_fsm_tb);
    end
  
    // End simulation after specified time
    initial begin
      #SIMULATION_TIME;
      $finish;
    end
  
endmodule // counter_fsm_tb

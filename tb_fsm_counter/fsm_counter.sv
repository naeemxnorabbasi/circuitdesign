// Counter
module counter_fsm (
  input wire clk,
  input wire reset,
  output reg [1:0] state
);

  // Define the states
  parameter ONE = 2'b00;
  parameter TWO = 2'b01;
  parameter THREE = 2'b10;
  parameter FOUR = 2'b11;

  // Define the next state logic
  always @(posedge clk or posedge reset) begin
    if (reset) begin
      state <= ONE; // Reset to state "one"
    end else begin
      case (state)
        ONE: state <= TWO;
        TWO: state <= THREE;
        THREE: state <= FOUR;
        FOUR: state <= ONE;
      endcase
    end
  end

endmodule

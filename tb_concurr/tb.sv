module tb;
   // parameters
   parameter PERIOD = 10;
   parameter HALF_PERIOD = PERIOD/2;
   parameter SIM_TIME = 120;

   // signal declaration
   logic reset, clk;
   logic a,b;

   // generate stimulus
   initial begin
      clk = 0;
      reset = 1;
      a = 0; 
      b = 0;
      #1;
     reset = 0;
     #14; 
     a = 1;
     #10;
     a = 0;
     b = 1;
     #10;
     b = 0;
     #20;
     a = 1;
     #10;
     a = 0;
     #20;
     a = 1;
     #10;
     a = 0;
     b = 1;
     #10;
     b = 0;     
   end  
  // generate clock
   always begin
      clk <= 0;
      #HALF_PERIOD;
      clk <= 1;
      #HALF_PERIOD;
   end

   // dump waves and end simulation
   initial begin
      $dumpfile("waves.vcd");
      $dumpvars;
      #SIM_TIME;
     $finish();
   end

  // instantiate DUT. In this case it contains the asertions. 
  dut dut_inst(.clk(clk), .reset(reset), .a(a), .b(b));
  
endmodule // tb

module dut(clk,reset,a,b);
  
  input logic clk, reset, a, b;
  
  // property declaration
  property if_a_then_b (clock, reset, a, b);
    @(posedge clock) disable iff (reset) ((a) |=> (b));
  endproperty

  // concurrent assertion
  asrt_if_a_then_b: assert property (if_a_then_b(clk, reset, a, b))  else 
    $error("a = %0d; b = %0d; at: %0t", $past(a), b, $time);
  
  // cover property
  cov_if_a_then_b: cover property (if_a_then_b(.clock(clk), .reset(reset), .a(a), .b(b)));

endmodule

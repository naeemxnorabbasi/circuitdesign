This directory contains a few examples.

1. tb - contains a simple test bench that generate clock and reset signals and dumps the waves in waves.vcd file.

2. tb_sys_func - contains a simple test bench that generates random stimulus and can be used to help us understand some of the system tasks $fatal, $error, ... etc.

3. tb_immediate - contains a simple test bench with an immediate assertion example.

4. tb_concurr - contains a simple test bench that contains a copncurrent assertion example.

5. tb_concurr2 - contains another simple example. Instead of coding the assertions inside DUT module, they are included in the tb.sv file. Actual content is the same as tb_concurr example.

6. tb_ast_bind - constains a simple example of a concurrent assertion with over lapping implication operator. The design.sv file constains the DUT and the assertions module. In the testbench, we bind the assertions module to the DUT.

7. tb_hello_world - contains a simple hello world example. Uses $display system function to print information on the terminal.

8. tb_mux_4_input - contains an structural AOI implementation of a four input mux. TB needs to be enhanced so that design functionality can be easily verified. 

These are working examples. Experiment with them and see if you can write your own assertions and check their syntax; generate the valid and invalid stimulus in an initial-begin-end using the "# t;" operator. See the example in the code. I find the following method helpful. From the english description draw timing diagram. From timing diagram write the boolean expression and sequences. Then write property declaration and finally direct the simulator using the assert or cover directives to do the verification.


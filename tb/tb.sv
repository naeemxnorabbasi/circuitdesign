// Generate Clock and Dump waves
module tb;
   
   parameter PERIOD = 10;
   parameter HALF_PERIOD = PERIOD/2;
   parameter SIM_TIME = 100;
   
   logic reset, clk;

   initial begin
      clk = 0;
      reset = 1;
   end

   always begin
      clk <= 0;
      #HALF_PERIOD/2;
      clk <= 1;
      #HALF_PERIOD/2;
   end

   initial begin
      $dumpfile("waves.vcd");
      $dumpvars;
      #SIM_TIME;
      $finish();
   end

// insert your property here



// insert your stimulus here



   
endmodule // tb







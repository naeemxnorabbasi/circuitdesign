// Use of system severity tasks $fatal, $error, $warning, $info, $display
// Immediate assertion
/*----------------------------
Demonstrate SVA system severity tasks.  

These are similar to $display system task. Can be used to report severity level associated with an assertion’s action_block while printing any user-defined message. 

1) $fatal reports a runtime fatal severity level and terminates the simulation with an error code.
2) $error reports a runtime error condition and does not terminate the simulation. $error is the default severity level.
3) $warning reports a runtime warning severity level and can be suppressed in a tool-specific manner.
4) $info reports any general assertion information, carries no specific severity, and can be used to capture functional coverage information during runtime.
------------------------------  */
 
module tb;
   
   parameter PERIOD = 10;
   parameter HALF_PERIOD = PERIOD/2;
   parameter SIM_TIME = 100;
   
   logic reset, clk;

   initial begin
      clk = 0;
      reset = 1;
   end

   always begin
      clk <= 0;
      #HALF_PERIOD;
      clk <= 1;
      #HALF_PERIOD;
   end

   
   initial begin
      $dumpfile("waves.vcd");
      $dumpvars;
      #SIM_TIME;
     $finish();
   end

// insert your stimulus here

  logic a, b;
// Note use of $urandom function for generating random stimulus when testing assertion code should be avoided. 
// I have come across a few strange timing issues becasue of which assertions were getting triggered incorrectly.
// In this case, we are only testing the system functions, so it is OK
   
  always @(posedge clk) begin
    a = $urandom();
    b = $urandom();
  end

// uncomment one line at a time in the following always block and observe the simulator output  
  always @ (a or b) begin
    // a_not_eq_b_fatal: assert (a != b) else $fatal("FATAL: a is equal to b at %0t", $time);
    // a_not_eq_b_error: assert (a != b) else $error("ERROR: a is equal to b at %0t", $time);
    // a_not_eq_b_warn:  assert (a != b) else $warning("WARNING: a is equal to b at %0t", $time);
    // a_not_eq_b_info:  assert (a != b) else $info("INFO: a is equal to b at %0t", $time);
    // a_not_eq_b_disp:  assert (a != b) else $display("MSG: a is equal to b at %0t", $time);
  end
  
endmodule // tb



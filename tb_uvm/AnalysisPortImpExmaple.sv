`include "uvm_macros.svh"
import uvm_pkg::*;

// producer class contains a uvm analysis port and sends an integer i to subscribers 1 and 2
class producer extends uvm_component;
    `uvm_component_utils(producer)

    integer i;

    uvm_analysis_port #(integer) send;

    function new(input string inst = "PROD", uvm_component c);
        super.new(inst,c);
        send = new("WRITE",this);
    endfunction

  virtual task run_phase(uvm_phase phase);
    phase.raise_objection(phase);
        for(i = 0; i < 10; i++) begin
            `uvm_info("PROD",$sformatf("Data Sent: %0d",i),UVM_NONE);
            send.write(i);
            #10;
        end
    phase.drop_objection(phase);
    endtask

endclass

class subscriber1 extends uvm_component;
    `uvm_component_utils(subscriber1)

    integer t;

    uvm_analysis_imp #(integer, subscriber1) recv1;

    function new(input string inst = "SUB1", uvm_component c);
        super.new(inst,c);
        recv1 = new("S1",this);
    endfunction

    virtual function void write(input integer t);
        `uvm_info("S1",$sformatf("Data Received: %0d", t), UVM_NONE);
    endfunction

endclass

class subscriber2 extends uvm_component;
    `uvm_component_utils(subscriber2)

    integer t;

    uvm_analysis_imp #(integer,subscriber2) recv2;

    function new(input string inst = "SUB2", uvm_component c);
        super.new(inst,c);
        recv2 = new("S2",this);
    endfunction

    virtual function void write(input integer t);
        `uvm_info("S2",$sformatf("Data Received: %0d", t), UVM_NONE);
    endfunction

endclass

class subscriber3 extends uvm_component;
    `uvm_component_utils(subscriber3)

    integer t;

    uvm_analysis_imp #(integer,subscriber3) recv3;

    function new(input string inst = "SUB3", uvm_component c);
        super.new(inst,c);
        recv3 = new("S3",this);
    endfunction

    virtual function void write(input integer t);
        `uvm_info("S3",$sformatf("Data Received: %0d", t), UVM_NONE);
    endfunction

endclass


class env extends uvm_env;
    `uvm_component_utils(env)

    producer p;
    subscriber1 s1;
    subscriber2 s2;

    function new (input string inst = "ENV", uvm_component c);
        super.new(inst,c);
    endfunction

    virtual function void build_phase(uvm_phase phase);
        super.build_phase(phase);
        p = producer::type_id::create("PROD", this);
        s1 = subscriber1::type_id::create("S1", this);
        s2 = subscriber2::type_id::create("S2", this);
    endfunction

    virtual function void connect_phase(uvm_phase phase);
        super.connect_phase(phase);
        p.send.connect(s1.recv1);
        p.send.connect(s2.recv2);
    endfunction

endclass

class env3 extends uvm_env;
    `uvm_component_utils(env3)

    producer p;
    subscriber1 s1;
    subscriber2 s2;
    subscriber3 s3;

    function new (input string inst = "ENV", uvm_component c);
        super.new(inst,c);
    endfunction

    virtual function void build_phase(uvm_phase phase);
        super.build_phase(phase);
        p = producer::type_id::create("PROD", this);
        s1 = subscriber1::type_id::create("S1", this);
        s2 = subscriber2::type_id::create("S2", this);
        s3 = subscriber3::type_id::create("S3", this);
    endfunction

    virtual function void connect_phase(uvm_phase phase);
        super.connect_phase(phase);
        p.send.connect(s1.recv1);
        p.send.connect(s2.recv2);
        p.send.connect(s3.recv3);
    endfunction

endclass


class test extends uvm_test;
    `uvm_component_utils(test)

    env e;

  function new(input string inst = "TEST", uvm_component c);
    super.new(inst,c);
    endfunction

    virtual function void build_phase(uvm_phase phase);
        super.build_phase(phase);
    	e = env::type_id::create("ENV",this);
    endfunction
  
endclass

class test3 extends uvm_test;
    `uvm_component_utils(test3)

    env3 e3;

  function new(input string inst = "TEST", uvm_component c);
    super.new(inst,c);
    endfunction

    virtual function void build_phase(uvm_phase phase);
        super.build_phase(phase);
    	e3 = env3::type_id::create("ENV",this);
    endfunction
  
endclass

/*
module tb;

    test t;

    initial begin
      t = new("TEST", null);
      run_test();
    end

endmodule
*/
module tb3;

    test3 t3;

    initial begin
      t3 = new("TEST", null);
      run_test();
    end

endmodule
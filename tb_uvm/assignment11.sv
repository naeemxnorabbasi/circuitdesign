// Assume you have producer consisting of three data members 
// ( reg [1:0] a = 2'b11, reg [3:0] b = 4'b0100 and 
// reg c = 1'b1 ). Communicate this data to three 
// subscribers with the help of analysis port. 
// Print the data sent by the producer as well as received by 
// all the subscribers for verification.
// Code your testbench here
// or browse Examples
// Assume you have producer consisting of three data members 
// ( reg [1:0] a = 2'b11, reg [3:0] b = 4'b0100 and 
// reg c = 1'b1 ). Communicate this data to three 
// subscribers with the help of analysis port. 
// Print the data sent by the producer as well as received by 
// all the subscribers for verification.

`include "uvm_macros.svh"
import uvm_pkg::*;

// transaction
class transaction extends uvm_sequence_item;

    rand reg [1:0] a = 2'b11;
    rand reg [3:0] b = 4'b0100;
    rand reg c = 1'b1;
    
    function new(input string trans = "TRANS");
      super.new(trans);
    endfunction

    `uvm_object_utils_begin(transaction)
    `uvm_field_int(a,UVM_DEFAULT)
    `uvm_field_int(b,UVM_DEFAULT)
    `uvm_field_int(c,UVM_DEFAULT)
    `uvm_object_utils_end

endclass

// producer
class producer extends uvm_component;
`uvm_component_utils(producer)

    transaction t;
    integer i;

    uvm_analysis_port #(transaction) send;

    function new(input string inst = "PROD", uvm_component c);
        super.new(inst,c);
        send = new("WRITE",this);
    endfunction

    virtual function void build_phase(uvm_phase phase);
        super.build_phase(phase);
        t = transaction::type_id::create("trans",this);
    endfunction

    virtual task run_phase(uvm_phase phase);
        phase.raise_objection(phase);
            for(i=0; i<10; i++) begin
                t.randomize();
                t.print(uvm_default_line_printer);
                send.write(t); 
            end
        phase.drop_objection(phase);
    endtask
endclass



// subscriber1, 2, and 3
class subscriber1 extends uvm_component;
    `uvm_component_utils(subscriber1)

    uvm_analysis_imp #(transaction, subscriber1) recv;

    function new(input string inst = "SUB1", uvm_component c);
        super.new(inst,c);
      recv = new("READ",this);
    endfunction

    virtual function void write(input transaction t);
        `uvm_info("SUB1","Data Recieved",UVM_NONE);
        t.print(uvm_default_line_printer);
    endfunction
endclass

class subscriber2 extends uvm_component;
    `uvm_component_utils(subscriber2)

    uvm_analysis_imp #(transaction, subscriber2) recv;

    function new(input string inst = "SUB2", uvm_component c);
        super.new(inst,c);
      recv = new("READ",this);
    endfunction

    virtual function void write(input transaction t);
        `uvm_info("SUB2","Data Recieved",UVM_NONE);
        t.print(uvm_default_line_printer);
    endfunction
endclass

class subscriber3 extends uvm_component;
    `uvm_component_utils(subscriber3)

    uvm_analysis_imp #(transaction, subscriber3) recv;

    function new(input string inst = "SUB3", uvm_component c);
        super.new(inst,c);
      recv = new("READ",this);
    endfunction

    virtual function void write(input transaction t);
        `uvm_info("SUB3","Data Recieved",UVM_NONE);
        t.print(uvm_default_line_printer);
    endfunction
endclass

// environment
class env extends uvm_env;
    `uvm_component_utils(env)

    producer p;
    subscriber1 s1;
    subscriber2 s2;
    subscriber3 s3;
    
    function new(input string inst = "ENV", uvm_component c);
        super.new(inst,c);
    endfunction

    virtual function void build_phase(uvm_phase phase);
        super.build_phase(phase);
        p = producer::type_id::create("PROD", this);
        s1 = subscriber1::type_id::create("SUB1", this);
        s2 = subscriber2::type_id::create("SUB2", this);
        s3 = subscriber3::type_id::create("SUB3", this);
    endfunction

    virtual function void connect_phase(uvm_phase phase);
        super.connect_phase(phase);
      p.send.connect(s1.recv);
      p.send.connect(s2.recv);
      p.send.connect(s3.recv);
    endfunction

endclass

// test
class test extends uvm_test;
`uvm_component_utils(test)

    env e;
    function new(input string inst = "TEST", uvm_component c);
        super.new(inst,c);
    endfunction

    virtual function void build_phase(uvm_phase phase);
        super.build_phase(phase);
        e = env::type_id::create("ENV", this);
    endfunction

endclass

// testbench
module tb;

    test t;

    initial begin
        t = new("TEST",null);
        run_test();
    end

endmodule

/*
Important Facts and Terminology
TLM Definitions:

TLM Port :  It is object that specify set of the methods such as put() to communicate with the other components.

TLM Exports : It is object taht provide the implementation of the methods specified by the port object.

connect(): TLM ports and exports are connected together with the help of the connect calls in the connect_phase (construction phase) of the environment class.

Producer :  Component responsible for producing sequence/transactions/data.

Consumer : Component responsible for processing/consuming the data received from the producer.

Initiator : Component which initiate the Transaction.

Tester :  Component which respond to the requset.

TLM mode of Operation:

PUT : Producer send a request to Consumer for initiating the Transaction (e.g. Monitor and Scoreboard)

GET: Consumer send a request to Producer to send the transaction. (e.g. Driver and Sequencer)

PEEK : Pass the Data without consumption (TLM_FIFO) 

One to Many : Send the Transaction to the Multiple Components (TLM Analysis port)

*/
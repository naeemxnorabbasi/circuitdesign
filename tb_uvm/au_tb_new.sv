// Questions for this assignment
// Design a testbench for the Combinational Circuit RTL mentioned in the Instruction tab.

// Code your design here
// DUT
module au
    (
      input [1:0] mode,
      input [3:0] a,b,
      output reg [7:0] y
    );
      
      always@(*)
        begin
          case(mode)
            2'b00: y = a + b;
            2'b01: y = a - b;
            2'b10: y = a * b;
            2'b11: y = a / b;
          endcase
        end
    endmodule  
// DUT Interface
interface au_if();
 logic [1:0] mode;
 logic [3:0] a;
 logic [3:0] b;
 logic [7:0] y;
endinterface



////////////////////UVM TESTBENCH//////
`include "uvm_macros.svh"
import uvm_pkg::*;

// transaction
class transaction extends uvm_sequence_item;
    rand logic [1:0] mode;
    rand logic [3:0] a;
    rand logic [3:0] b;
    logic [7:0] y;
   
  function new(input string inst = "TRANS");
        super.new(inst);
    endfunction

    `uvm_object_utils_begin(transaction)
    `uvm_field_int(mode,UVM_DEFAULT)
    `uvm_field_int(a,UVM_DEFAULT)
    `uvm_field_int(b,UVM_DEFAULT)
    `uvm_field_int(y,UVM_DEFAULT)
    `uvm_object_utils_end

endclass


// generator
class generator extends uvm_sequence #(transaction);
    `uvm_object_utils(generator)

    transaction t;
    integer i;

    function new(input string inst ="GEN");
        super.new(inst);
    endfunction
    
    virtual task body();
      t = transaction::type_id::create("TRANS");
    for(i=0;i<10;i++) begin
      start_item(t);
      t.randomize();
      `uvm_info("GEN",$sformatf("Data sent to Driver: a: %0d, b: %0d, mode: %0d",t.a,t.b,t.mode),UVM_NONE);
      t.print(uvm_default_line_printer);
      finish_item(t);
      #10;
    end
    endtask
        
endclass

// driver
class driver extends uvm_driver #(transaction);
    `uvm_component_utils(driver)

    transaction data;
    virtual au_if aif;

    function new(input string inst ="DRV", uvm_component c);
        super.new(inst,c);
    endfunction

    virtual function void build_phase(uvm_phase phase);
        super.build_phase(phase);
        data = transaction::type_id::create("TRANS");
      if(!uvm_config_db #(virtual au_if)::get(this,"","aif",aif)) `uvm_info("DRV","Unable to read config DB",UVM_NONE);
    endfunction

    virtual task run_phase(uvm_phase phase);
        forever begin
            seq_item_port.get_next_item(data);
          `uvm_info("DRV",$sformatf("Data sent to interface, a: %0d, b: %0d, mode: %0d",data.a,data.b,data.mode),UVM_NONE);
            aif.a=data.a;
            aif.b=data.b;
            aif.mode=data.mode;
            data.print(uvm_default_line_printer);
            seq_item_port.item_done();
        end
    endtask

endclass

//monitor
class monitor extends uvm_monitor;
    `uvm_component_utils(monitor)

    transaction t;
    virtual au_if aif;

    uvm_analysis_port #(transaction) send;

    function new(input string inst = "MON", uvm_component c);
        super.new(inst,c);
        send = new("WRITE",this);
    endfunction

    virtual function void build_phase(uvm_phase phase);
        super.build_phase(phase);
        t = transaction::type_id::create("MON");
        if(!uvm_config_db #(virtual au_if)::get(this,"","aif", aif)) 
            `uvm_info("MON",$sformatf("Data recived from DUT, a: %0d, b: %0d, mode: %0d, y: %0d",t.a,t.b,t.mode,t.y),UVM_NONE);
    endfunction

    virtual task run_phase(uvm_phase phase);
        forever begin 
        #10;
        t.a = aif.a;
        t.b = aif.b;
        t.mode = aif.mode;
        t.y = aif.y;
          `uvm_info("MON",$sformatf("Data send to scoreboard: a: %0d, b: %0d, mode: %0d, y: %0d ",t.a,t.b,t.mode,t.y),UVM_NONE);
        send.write(t);
        t.print(uvm_default_line_printer);
        end
    endtask
endclass

// agent
class agent extends uvm_agent;
    `uvm_component_utils(agent)

    driver d;
    monitor m;
    uvm_sequencer #(transaction) seq;

    
    function new(input string inst = "AGENT", uvm_component c);
        super.new(inst,c);
    endfunction

    virtual function void build_phase(uvm_phase phase);
        super.build_phase(phase);
        d = driver::type_id::create("DRV",this);
        m = monitor::type_id::create("MON",this);
        seq = uvm_sequencer #(transaction)::type_id::create("SEQ",this);
    endfunction

    virtual function void connect_phase(uvm_phase phase);
        super.connect_phase(phase);
        d.seq_item_port.connect(seq.seq_item_export);
    endfunction

endclass

// scoreboard
class scoreboard extends uvm_scoreboard;
    `uvm_component_utils(scoreboard)

    transaction data;

    uvm_analysis_imp #(transaction,scoreboard) recv;

    function new(input string inst = "SCO", uvm_component c);
        super.new(inst,c);
        recv = new("Read",this);
    endfunction

    virtual function void build_phase(uvm_phase phase);
        super.build_phase(phase);
        data = transaction::type_id::create("TRANS",this);
    endfunction

  
  /*
    virtual function void write(input transaction t);
        data = t;
        data.print(uvm_default_line_printer);
        //
        case(data.mode)
            2'b00: begin if(data.y == data.a + data.b) `uvm_info("SCO","Test PASSED",UVM_NONE); else `uvm_info("SCO","Test FAILED",UVM_NONE); end
            2'b01: begin if(data.y == data.a - data.b) 
                `uvm_info("SCO","Test PASSED",UVM_NONE); 
              else
                `uvm_info("SCO","Test FAILED",UVM_NONE);
            end
            2'b10: begin
              if(data.y == data.a * data.b)
                `uvm_info("SCO","Test PASSED",UVM_NONE);
              else
                `uvm_info("SCO","Test FAILED",UVM_NONE);
            end
            2'b11: begin
              if(data.y == data.a / data.b)
                `uvm_info("SCO","Test PASSED",UVM_NONE);
              else
                `uvm_info("SCO","Test FAILED",UVM_NONE);
            end
          endcase
    endfunction
*/
  
  virtual function void write(input transaction t);
    data = t;
    data.print(uvm_default_line_printer); 
    
    case(data.mode)
        2'b00: begin 
            if(data.y == data.a + data.b) `uvm_info("SCO","Test PASSED",UVM_NONE)
            else `uvm_info("SCO","Test FAILED",UVM_NONE);
        end
        2'b01: begin 
            if(data.y == data.a - data.b) 
                `uvm_info("SCO","Test PASSED",UVM_NONE)
            else
                `uvm_info("SCO","Test FAILED",UVM_NONE);
        end
        2'b10: begin
            if(data.y == data.a * data.b)
                `uvm_info("SCO","Test PASSED",UVM_NONE)
            else
                `uvm_info("SCO","Test FAILED",UVM_NONE);
        end
        2'b11: begin
            if(data.y == data.a / data.b)
                `uvm_info("SCO","Test PASSED",UVM_NONE)
            else
                `uvm_info("SCO","Test FAILED",UVM_NONE);
        end
    endcase
endfunction

  
  
endclass

// environment
class env extends uvm_env;
    `uvm_component_utils(env)

    function new(input string inst="ENV", uvm_component c);
        super.new(inst,c);
    endfunction

    scoreboard s;
    agent a;
    
    virtual function void build_phase(uvm_phase phase);
        super.build_phase(phase);
        s = scoreboard::type_id::create("SCO",this);
        a = agent::type_id::create("AGENT", this);
    endfunction

    virtual function void connect_phase(uvm_phase phase);
        super.connect_phase(phase);
        a.m.send.connect(s.recv);
    endfunction

endclass


// test
class test extends uvm_test;
    `uvm_component_utils(test)

    function new(input string inst = "TEST", uvm_component c);
        super.new(inst,c);
    endfunction

    generator g;
    env e;


    virtual function void build_phase(uvm_phase phase);
        super.build_phase(phase);
        g = generator::type_id::create("GEN",this);
        e = env::type_id::create("ENV",this);
    endfunction

    virtual task run_phase(uvm_phase phase);
        phase.raise_objection(phase);
        g.start(e.a.seq);
      phase.drop_objection(phase);
    endtask

endclass


// testbench
module tb;
  
  test t;
  au_if aif();
  au dut(.a(aif.a), .b(aif.b), .mode(aif.mode),.y(aif.y));

    initial begin
        $dumpvars;
        $dumpwaves("waves.vcd");
        uvm_config_db #(virtual au_if)::set(null,"*","aif",aif);
        t = new("TEST", null);
        uvm_top.enable_print_topology = 1;
        run_test();
    end

endmodule

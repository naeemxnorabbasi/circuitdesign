// libraires and imports
`include "uvm_macros.svh"
import uvm_pkg::*;


// Producer
class producer extends uvm_component;
// register the class with factory
`uvm_component_utils(producer)
// data to send
   int a = 12;

// create a uvm blocking port named "send"
   uvm_blocking_put_port #(int) send;
  
// constructor
   function new(input string inst, uvm_component c);
      super.new(inst,c);
      //`uvm_info("PROD","Producer",UVM_NONE);
      send = new("PUT",this);
   endfunction // new

// run task puts data
   task run();
     `uvm_info("PROD",$sformatf("Data Sent to Consumer: %0d", a), UVM_NONE);
      send.put(a);
   endtask // run
   
endclass // producer


// Consumer
class consumer extends uvm_component;
// register the class with the factory
`uvm_component_utils(consumer)

// create a uvm blocking port named "send"
   uvm_blocking_put_imp #(int, consumer) recv;
  
// constructor
   function new(input string inst, uvm_component c);
      super.new(inst,c);
      //`uvm_info("PROD","Producer",UVM_NONE);
      recv = new("CONS",this);
   endfunction // new

// put task
   virtual task put(input int t);
     `uvm_info("CONS",$sformatf("Data Received from producer: %0d",t),UVM_NONE);  
   endtask // put
   
endclass // consumer


// Env
class env extends uvm_env;
// register the class with the factory
   `uvm_component_utils(env)
   
// create instaces of producer and consumer
   producer p;
   consumer c;
   
// constructor
   function new(input string inst, uvm_component c);
      super.new(inst,c);
   endfunction // new

// Build Phase
   virtual function void build_phase(uvm_phase phase);
      super.build_phase(phase);
      p = producer::type_id::create("PROD",this);
      c = consumer::type_id::create("CONS",this);
   endfunction // build_phase
   
// Connect Phase
   virtual function void connect_phase(uvm_phase phase);
      super.connect_phase(phase);
      p.send.connect(c.recv);
   endfunction // connect_phase

// end of elaboration phase - this shall print TB hierarchy from ENV
  virtual function void end_of_elaboration_phase(uvm_phase phase);
    super.end_of_elaboration_phase(phase);
    print();
  endfunction
   
endclass // env


// Test
class test extends uvm_component;
// register the class with the factory
   `uvm_component_utils(test)

// create an instance of environment   
   env e;

// constructor for test
   function new(input string inst, uvm_component c);
      super.new(inst,c);
   endfunction // new

// build phase 
   virtual function void build_phase(uvm_phase phase);
      super.build_phase(phase);
      e = env::type_id::create("ENV",this);
   endfunction // build_phase

// end of elaboration phase - this shall print the TB hierarchy from TEST
  virtual function void end_of_elaboration_phase(uvm_phase phase);
    super.end_of_elaboration_phase(phase);
    print();
  endfunction
 
// run phase
   virtual task run_phase(uvm_phase phase);
      #100;
      global_stop_request();    
   endtask // run
   
endclass // test


// TB

module tb();

   test t;

   initial begin
      t = new("TEST", null);
      run_test();
   end
   

endmodule // tb



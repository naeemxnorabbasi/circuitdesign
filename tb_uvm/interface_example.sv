
// Sequential Circuit
module top(a,b,y,clk,reset);
    input logic [3:0] a;
    input logic [3:0] b;
    input logic clk, reset;
    output logic [4:0] y;
  
    always @(posedge clk or posedge reset) begin
        if(reset) begin
            y <= 0;
        end else begin
         y <= a + b;
        end
    end
    
  endmodule
  
  interface top_if;
  logic [3:0] a;
  logic [3:0] b;
  logic [4:0] y;
  logic clk;
  logic reset;
  endinterface
  
  module tb;
  // instantiate dut and its interface
    top_if tif();
    top dut(.a(tif.a), .b(tif.b), .y(tif.y), .clk(tif.clk), .reset(tif.reset));
  // initialize signals
  initial begin
      tif.clk = 0;
      tif.reset=1;
      tif.a = 4'b0100;
      tif.b = 4'b0111;
      #1;
      tif.reset = 0;
    #100;
    $finish();
  end
 // generate clock   
  always #5 tif.clk = ~tif.clk;
// dump waves
    initial begin
      $dumpvars;
      $dumpfile("waves.vcd");
    end
  
  endmodule

// Combinational cicuit
/*
// Example 1
module top(a,b,y);
    input logic [3:0] a;
    input logic [3:0] b;
    output logic [4:0] y;
  
    assign y = a + b;
    
  endmodule
  
  interface top_if;
  logic [3:0] a;
  logic [3:0] b;
  logic [4:0] y;
  endinterface
  
  module tb;
  
    top_if tif();
  
      top dut(.a(tif.a), .b(tif.b), .y(tif.y));
  
  initial begin
      tif.a = 4'b0100;
      tif.b = 4'b0111;
    #1;
    $finish();
  end
    
    initial begin
      $dumpvars;
      $dumpfile("waves.vcd");
    end
  
  endmodule


  */
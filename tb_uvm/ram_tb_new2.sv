// design
module ram(
    input clk,wr,
    input [7:0] din,
    output reg [7:0] dout,
    input [3:0] addr
        );
    reg [7:0] mem [15:0];  
      
  initial begin
    for (integer i = 0; i< 16; i++) begin
      mem[i] = 0;
    end
  end
  
    always@(posedge clk)
    begin
    if(wr == 1'b1)
      mem[addr] <= din;
    else
      dout <= mem[addr];
    end  
endmodule

// interface
   ////////////////////////////////////////////////////////////
    interface ram_if();
        logic clk;
        logic wr;
        logic [7:0] din;
        logic [3:0] addr;
        logic [7:0] dout;
        endinterface
         
///////Test Bench///////
`include "uvm_macros.svh"
import uvm_pkg::*;

// transaction
class transaction extends uvm_sequence_item;
 
    rand logic wr;
    rand logic [7:0] din;
    rand logic [3:0] addr;
    logic [7:0] dout;

  constraint addr_c {addr > 2; addr < 8;};
  
  
    function new(input string inst ="TRANS");
        super.new(inst);
    endfunction

    `uvm_object_utils_begin(transaction)
    `uvm_field_int(wr,UVM_DEFAULT)
    `uvm_field_int(din,UVM_DEFAULT)
    `uvm_field_int(dout,UVM_DEFAULT)
    `uvm_field_int(addr,UVM_DEFAULT)
    `uvm_object_utils_end

endclass

// generator
class generator extends uvm_sequence #(transaction);
    `uvm_object_utils(generator)

    transaction t;
    integer i;

    function new(input string inst = "GEN");
        super.new(inst);
    endfunction

    virtual task body();
        t = transaction::type_id::create("TRANS");
      for(i=0; i<50; i++) begin
            start_item(t);
            t.randomize();
            t.print(uvm_default_line_printer);
            `uvm_info("GEN",$sformatf("Data sent to Driver: wr: %0d, din: %0d, addr: %0d",t.wr,t.din,t.addr),UVM_NONE);
            finish_item(t);
        //#20;
        end
    endtask

endclass

// driver
class driver extends uvm_driver #(transaction);
    `uvm_component_utils(driver)

    function new(input string inst = "DRV", uvm_component c);
        super.new(inst,c);
    endfunction

    transaction data;
    virtual ram_if rif;

    virtual function void build_phase(uvm_phase phase);
        super.build_phase(phase);
        data = transaction::type_id::create("TRANS");
        if(!uvm_config_db #(virtual ram_if)::get(this,"","rif",rif)) `uvm_info("DRV","Unable to read Config DB",UVM_NONE);
    endfunction

    virtual task run_phase(uvm_phase phase);
        forever begin
          seq_item_port.get_next_item(data);
            rif.wr = data.wr;
            rif.din = data.din;
            rif.addr = data.addr;
            `uvm_info("DRV",$sformatf("Data sent to RAM interface: wr: %0d, din: %0d, addr: %0d",rif.wr, rif.din,rif.addr),UVM_NONE);
            data.print(uvm_default_line_printer);
          seq_item_port.item_done();
          @(posedge rif.clk);    
          if(data.wr == 1'b0) @(posedge rif.clk);
        end
    endtask

endclass

// monitor
class monitor extends uvm_monitor;
    `uvm_component_utils(monitor)

    transaction t;
    virtual ram_if rif;
  
    uvm_analysis_port #(transaction) send;

    function new(input string inst = "MON", uvm_component c);
        super.new(inst,c);
        send = new("Write",this);
    endfunction

    virtual function void build_phase(uvm_phase phase);
        super.build_phase(phase);
        t = transaction::type_id::create("TRANS");
        if(!uvm_config_db #(virtual ram_if)::get(this,"","rif",rif)) `uvm_info("DRV","Unable to read Config DB",UVM_NONE);
    endfunction

    virtual task run_phase(uvm_phase phase);
        forever begin
          @(posedge rif.clk);
          `uvm_info("MON",$sformatf("Data read from RAM interface: wr: %0d, din: %0d, addr: %0d, dout: %0d",rif.wr,rif.din,rif.addr,rif.dout),UVM_NONE);
            t.wr = rif.wr;
            t.din = rif.din;
            t.addr = rif.addr;
          	if(rif.wr == 1'b0) begin
            	@(posedge rif.clk);
            	t.dout = rif.dout;
          	end
            send.write(t);
            t.print(uvm_default_line_printer);
        end
    endtask

endclass


// scoreboard
class scoreboard extends uvm_scoreboard;
    `uvm_component_utils(scoreboard)
    transaction data;

    uvm_analysis_imp #(transaction,scoreboard) recv;

    reg [7:0] tarr[20] = '{default:0};
  
    function new(input string inst = "SCO", uvm_component c);
        super.new(inst,c);
        recv = new("Read",this);
    endfunction

    virtual function void build_phase(uvm_phase phase);
        super.build_phase(phase);
        data = transaction::type_id::create("TRANS");
    endfunction
    // checking mechanism
  virtual function void write(transaction data);
    `uvm_info("SCO","Data rcvd from Monitor", UVM_NONE);
    data.print(uvm_default_line_printer);
 
    if(data.wr == 1'b1) begin
		tarr[data.addr] = data.din;
		`uvm_info("SCO", $sformatf("Data Write Oper din :%0h and tarr[data.addr] : %0h ", data.din, tarr[data.addr]), UVM_NONE);
    end
   
    if(data.wr == 1'b0) begin
      if(data.dout == tarr[data.addr]) `uvm_info("SCO", "Test Passed ", UVM_NONE) else `uvm_error("SCO", "Test Failed");
 	`uvm_info("SCO", $sformatf("Data Read Oper dout :%0h and tarr[data.addr] : %0h ", data.dout, tarr[data.addr]), UVM_NONE);
    end
endfunction  
  
endclass

// agent
class agent extends uvm_agent;
    `uvm_component_utils(agent)

    function new(input string inst = "AGENT", uvm_component c);
        super.new(inst,c);
    endfunction

    driver d;
    monitor m;
    uvm_sequencer #(transaction) seq;
    
    virtual function void build_phase(uvm_phase phase);
        super.build_phase(phase);
        d = driver::type_id::create("DRV",this);
        m = monitor::type_id::create("MON",this);
        seq = uvm_sequencer #(transaction)::type_id::create("SEQ",this);                
    endfunction

    virtual function void connect_phase(uvm_phase phase);
        super.connect_phase(phase);
        d.seq_item_port.connect(seq.seq_item_export);
    endfunction

endclass

// env
class env extends uvm_env;
    `uvm_component_utils(env)

    function new(input string inst = "ENV", uvm_component c);
        super.new(inst,c);
    endfunction

    agent a;
    scoreboard s;

    virtual function void build_phase(uvm_phase phase);
        super.build_phase(phase);
        a = agent::type_id::create("AGENT",this);
        s = scoreboard::type_id::create("SCO",this);
    endfunction

    virtual function void connect_phase(uvm_phase phase);
        super.connect_phase(phase);
        a.m.send.connect(s.recv);
    endfunction

endclass

// test
class test extends uvm_test;
    `uvm_component_utils(test)

    function new(input string inst = "TEST", uvm_component c);
        super.new(inst,c);
    endfunction

    generator g;
    env e;

    virtual function void build_phase(uvm_phase phase);
        super.build_phase(phase);
        g = generator::type_id::create("GEN",this);
        e = env::type_id::create("ENV",this);
    endfunction

    virtual task run_phase(uvm_phase phase);
        phase.raise_objection(phase);
        g.start(e.a.seq);
        phase.drop_objection(phase);
    endtask

endclass

//tb
module tb;

    test t;
    ram_if rif();
    ram dut (.clk(rif.clk), .wr(rif.wr), .din(rif.din), .addr(rif.addr), .dout(rif.dout));

    initial begin
        rif.clk = 0;
    end

    always #10 rif.clk = ~rif.clk;

    initial begin
        $dumpvars;
        $dumpfile("waves.vcd");
        t = new("TEST",null);
        uvm_config_db #(virtual ram_if)::set(null,"*","rif",rif);
        uvm_top.enable_print_topology = 1;
        run_test();
    end


endmodule


// load libraries
`include "uvm_macros.svh"
import uvm_pkg::*;

// purpose is to send a transaction, a data packet from producer to consumer block and learn about the UVM features for synchronization and communications
// also experiment with the macros that allow printing of the information
// also experiment with the build, connect, end of elaboration, run and report phases.

// transaction class, derived from uvm_sequence_item, which is a uvm_component
class transaction extends uvm_sequence_item;
// register the class with the factory
//   `uvm_component_utils(transaction)

// define two random variables
   rand bit [7:0] din = 8'h12;
   rand bit [15:0] ctrl = 16'hffff;

// class constructor 
   function new(input string inst = "TRANS");
      super.new(inst);
   endfunction // new

// priting the data items of the transaction
   `uvm_object_utils_begin(transaction)
   `uvm_field_int(din, UVM_DEFAULT)
   `uvm_field_int(ctrl, UVM_DEFAULT)
   `uvm_object_utils_end
   
endclass // transaction



// producer
class producer extends uvm_component;
// register class with the factory
   `uvm_component_utils(producer)

// instantiate the transaction class
   transaction t;

// add a TLM put port called send. done using a parametrized class
   uvm_blocking_put_port #(transaction) send;

// build phase
   virtual function void build_phase(uvm_phase phase);
      super.build_phase(phase);
     t = transaction::type_id::create("TRANS", this);
    endfunction // build_phase

// constructor
   function new(input string inst = "PROD", uvm_component c);
      super.new(inst,c);
      send = new("PUT", this);
   endfunction // new


// run - print the random data    
   task run();
      t.randomize();
      t.print();
      send.put(t);
   endtask // run
   
endclass // producer


// consumer
class consumer extends uvm_component;
// register the class with the factory
`uvm_component_utils(consumer)
   
// create TLM port, put imp - parameterized class
   uvm_blocking_put_imp #(transaction, consumer) recv;
   

// create constructor for consumer as well as the TLM blocking port
   function new(input string inst = "CONS", uvm_component c);
      super.new(inst,c);
      recv = new("RECV", this);
   endfunction // new

// build phase
//   virtual function void build_phase(uvm_phase phase);
//      super.build_phase(phase);
//      recv = transaction::type_id::create("RECV",this);
//   endfunction // build_phase
   
// print the transaction class.
  virtual task put(input transaction t);
      t.print();
   endtask // put
   

endclass // consumer


// environment
class env extends uvm_env;
// register with the factor
   `uvm_component_utils(env)

// declare producer and consumer
   producer p;
   consumer c;

// constructor
   function new(input string inst = "ENV", uvm_component c);
      super.new(inst,c);
      //p = new("PROD", this);
      //c = new("CONS", this);
   endfunction // new
   
// build phase
   virtual function void build_phase(uvm_phase phase);
      super.build_phase(phase);
      p = producer::type_id::create("PROD",this);
      c = producer::type_id::create("CONS",this);      
   endfunction // build_phase
   
// connect phase
   virtual function void connect_phase(uvm_phase phase);
      super.connect_phase(phase);
      p.send.connect(c.recv);
   endfunction // connect_phase
   
endclass // env



// test
class test extends uvm_test();
// register with the factory
`uvm_component_utils(test)
   
// instantiate the environment in the test
   env e;

// constructor
   function new(input string inst = "TEST", uvm_component c);
      super.new(inst, c);
      //e =new("ENV", this);
   endfunction // new
   
// build phase
   virtual function void build_phase(uvm_phase phase);
      super.build_phase(phase);
      e = env::type_id::create("ENV",this);
   endfunction // build_phase
   
      
// end of elaboration phase
//   virtual function void end_of_elboration_phase(uvm_phase phase);
//     super.end_of_elaboration_phase(phase);
//      print();
//   endfunction // end_of_elboration_phase
   
// run phase
  virtual task run_phase(uvm_phase phase);
      #100;
      global_stop_request();
   endtask // run_phase
   
   
endclass // test



//test bench
module tb();

   test t;

   initial begin
      t = new("TEST_DUMMY", null);
      run_test();
   end
  
endmodule // tb





//////////////////////////////////

// load libraries
`include "uvm_macros.svh"
import uvm_pkg::*;

// purpose is to send a transaction, a data packet from producer to consumer block and learn about the UVM features for synchronization and communications
// also experiment with the macros that allow printing of the information
// also experiment with the build, connect, end of elaboration, run and report phases.

// transaction class, derived from uvm_sequence_item, which is a uvm_component
class transaction extends uvm_sequence_item;
// register the class with the factory
   `uvm_component_utils(transaction)

// define two random variables
   rand reg [7:0] din = 8'h12;
   rand reg [15:0] ctrl = 16'hffff;

// class constructor 
   function new(input string inst = "TRANS");
      super.new(inst);
   endfunction // new

// priting the data items of the transaction
   `uvm_object_utils_begin(transaction)
   `uvm_field_int(din, UVM_DEFAULT)
   `uvm_field_int(ctrl, UVM_DEFAULT)
   `uvm_object_utils_end
   
endclass // transaction



// producer
class producer extends uvm_component;
// register class with the factory
   `uvm_componnet_utils(producer)

// instantiate the transaction class
   transaction t;

// add a TLM put port called send. done using a parametrized class
   uvm_blocking_put_port #(transaction) send;
   
// constructor
   function new(input string inst = "PROD", uvm_component c);
      super.new(inst,c);
      send = new("PUT", this);
   endfunction // new

// build phase
   virtual function void build_phase(uvm_phase phase);
      super.new(phase);
      t = transaction::type_id::create("TRANS", this);
    endfunction // build_phase

// run - print the random data    
   task run();
      t.randomize();
      t.print();
      send.put(t);
   endtask // run
   
endclass // producer


// consumer
class consumer extends uvm_component;
// register the class with the factory
`uvm_component_utils(consumer)
   
// create TLM port, put imp - parameterized class
   uvm_blocking_put_imp #(transaction, consumer) recv;
   

// create constructor for consumer as well as the TLM blocking port
   function new(input string inst = "CONS", uvm_component c);
      super.new(inst,c);
      recv = new("RECV", this);
   endfunction // new

// build phase
   virtual function void build_phase(uvm_phase phase);
      super.new(phase);
      recv = transaction::type_id::create("RECV",this);
   endfunction // build_phase
   
// print the transaction class.
   task put(transaction t);
      t.print();
   endtask // put
   

endclass // consumer


// environment
class env extends uvm_env;
// register with the factor
   `uvm_component_utils(env)

// declare producer and consumer
   producer p;
   consumer c;

// constructor
   function new(input string inst = "ENV", uvm_component c);
      super.new(inst,c);
      p = new("PROD", this);
      c = new("CONS", this);
   endfunction // new
   
// build phase
   virtual function void build_phase(uvm_phase phase);
      super.new(phase);
      p = producer::type_id::create("PROD",this);
      c = producer::type_id::create("CONS",this);      
   endfunction // build_phase
   
// connect phase
   virtual function void connect_phase(uvm_phase phase);
      super.new(phase);
      p.send.connect(c.recv);
   endfunction // connect_phase
   
endclass // env



// test
class test extends uvm_test();
// register with the factory
`uvm_component_utils(test)
   
// instantiate the environment in the test
   env e;

// constructor
   function new(input string inst = "TEST", uvm_component c);
      super.new(inst, c);
      e =new("ENV", this);
   endfunction // new
   
// build phase
   virtual function void build_phase(uvm_phase phase);
      super.new(phase);
      e = env::type_id::create("ENV",this);
   endfunction // build_phase
   
      
// end of elaboration phase
   virtual function void end_of_elboration_phase(uvm_phase phase);
      super.new(phase);
      print();
   endfunction // end_of_elboration_phase
   
// run phase
   virtual task run_phase();
      #100;
      global_stop_request();
   endtask // run_phase
   
   
endclass // test



//test bench
module tb();

   test t;

   initial begin
      t = new("TEST_DUMMY", null);
      run_test();
   end
  
endmodule // tb







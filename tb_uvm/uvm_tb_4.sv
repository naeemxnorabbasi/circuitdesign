`include "uvm_macros.svh"
import uvm_pkg::*;

// this test bench uses transaction derived from uvm_sequence_item, registers it with uvm_object_utils_begin(), uvm_object_utils_end, and uvm_field_int to register and to print the data
// alternate way to register is to use `uvm_component_utils() macro


// transaction
// contains two data items
class transaction extends uvm_sequence_item;
// data items
   integer a = 53;
   integer b = 111;
// constructor for the class
   function new(input string inst = "TRANS");
      super.new(transaction);
   endfunction // new
// registering the data items with the factory
   `uvm_object_utils_begin(transaction)
      `uvm_field_int(a, UVM_NONE);
      `uvm_field_int(b, UVM_NONE);      
   `uvm_object_utils_end
       
endclass // transaction

// producer
// register with the factory
class producer extends uvm_component;
   `uvm_component_utils(producer)

// instantiate a transaciton class
   transaction t;
      
// adding a blocking put port, called send
   uvm_blocking_put_port #(transaction) send;
   
   function new(input string inst = "PROD", uvm_component c);
      super.new(inst,c);
      send = new("PUT",this);     
   endfunction // new

   virtual function void build_phase(uvm_phase phase);
      super.build_phase(phase);
      t = transaction""type_id::create("TRANS", this);
   endfunction // build_phase

// now send the transaction
   virtual task run_phase(uvm_phase phase);
      phase.raise_objection(phase);
      for (i = 0; i<10; i++) begin
//	 `uvm_info("PROD", $sformatf("Data Sent : %0d", i), UVM_NONE);
	 send.put(transaction);
	 #10; // this is done for synachronization, this is something I do not fully understand.
	 end
      phase.drop_objection(phase);
   endtask // run_phase
   
endclass // producer


// consumer
// register with the factory
class consumer extends uvm_component;
`uvm_component_utils(consumer)

   integer
   
// adding a blocking get port, called recv
   uvm_blocking_put_imp #(transaction,consumer) recv;

   function new(input string inst = "CONS", uvm_component c);
      super.new(inst,c);
      send = new("RECV",this);     
   endfunction // new

   virtual function void build_phase(uvm_phase phase);
      super.build_phase(phase);
   endfunction // build_phase

// now get the transaction
   virtual task run_phase(uvm_phase phase);
      phase.raise_objection(phase);
      for (i = 0; i<10; i++) begin
	 `uvm_info("PROD", $sformatf("Data Received : %0d", i), UVM_NONE);
	 send.put(transaction);
	 #10; // this is done for synachronization, this is something I do not fully understand.
	 end
      phase.drop_objection(phase);
   endtask // run_phase


   
   
endclass // consumer



// env
// register with the factory




// test
// register with the factory



//testbench
// instatiate the test and run the test









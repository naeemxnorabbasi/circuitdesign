// Questions for this assignment
// Design a testbench for the Combinational Circuit RTL mentioned in the Instruction tab.



////////////////////////////////////////////////////
////////TEST BENCH SKELETON/////
////////////////////////////////////////////////////

// transaction class
/*
uvm_sequence_item —> uvm_object
Keep track of all inputs and outputs
Use modifiers for inputs and contro, rand or rand
Do not use modifiers for outputs
Register data members with the factory using object_utils
*/

// generator
/*
uvm_sequence #(type of data object, e.g., transaction)—> uvm_object

Register with the factory using uvm object utils

Declare transaction and any other local variables.

Constructor for the generator

Generate random transactions for DUT - use virtual task body and in it create a transaction object. Use loops as needed for generating multiple transactions. 
Use start_item() and finish_item() for sending data from sequencer to driver. 
start_item() —> we have a sequence to send
Generate sequence
finsih_item() —> we have sent the sequence to the driver. 

virtual task body();
t = transaction::type_id::create();
for(i=0;i<10;i++) begin
start_item(t);
t.randomize();
`uvm_info(“”GEN,$sformatf(”Data sent to Driver: a: %0d, b: %0d, mode: %0d”,a,b,mode),UVM_NONE);
t.print();
finish_item(t);
#10;
end
endtask

For combinational circuits, send the data first and then add delay for correct phasing.
*/


// driver
/*
uvm_driver #(type of data object) —> uvm_component

Get stimulus from generator and apply to the DUT using the interface
Access interface with config_db
get_next_item —> get next item from uvm_sequence
item_done —> request completed, ready to receive new request.
Apply data to DUT with virtual interface.

*/
//monitor
/*
uvm_monitor ---> uvm_component

Receive data from DUT using interface and send it to scoreboard
Update data container with data received from virtual interface
Use analysis port to send data to scoreboard
*/

// scoreboard
/*
uvm_scoreboard —> uvm_component

Receive data/transaction from monitor and compare with golden data
Use implementation of analysis port
*/

// agent
/*
uvm_agent —> uvm_component

Create instances of driver, sequencer, and monitor
Connect driver and sequencer

Driver —> initiator
Sequencer —> Target

*/

///env
/*
uvm_env —> uvm_component

Create instances of agent and scoreboard
Connect analysis port of monitor and scoreboard
Monitor —> Initiator
Scoreboard —> Target

*/


// test
/*
uvm_test —> uvm_component

Create instances of environment
Connector generator and sequencer

Choose specific sequence to send to the driver
Generator —> Sequencer

*/

// testbench
/*
Create test instance
Create interface instance
Connect DUT to interface
Set virtual interface in uvm_config_db

*/


// DUT
module au
    (
      input [1:0] mode,
      input [3:0] a,b,
      output reg [7:0] y
    );
      
      always@(*)
        begin
          case(mode)
            2'b00: y = a + b;
            2'b01: y = a - b;
            2'b10: y = a * b;
            2'b11: y = a / b;
          endcase
        end
    endmodule  
// DUT Interface
interface au_if();
 logic [1:0] mode;
 logic [3:0] a;
 logic [3:0] b;
 logic [7:0] y;
endinterface

////////////////////UVM TESTBENCH//////
`include "uvm_macros.svh"
import uvm_pkg::*;

// transaction
// transaction class
/*
uvm_sequence_item —> uvm_object
Keep track of all inputs and outputs
Use modifiers for inputs and contro, rand or rand
Do not use modifiers for outputs
Register data members with the factory using object_utils
*/

class transaction extends uvm_sequence_item;
    rand logic [1:0] mode;
    rand logic [3:0] a;
    rand logic [3:0] b;
    logic [7:0] y;
   
    function new(inout string inst = "TRANS");
        super.new(inst);
    endfunction

    `uvm_object_utils_begin(transaction)
    `uvm_field_int(mode,UVM_DEFAULT)
    `uvm_field_int(a,UVM_DEFAULT)
    `uvm_field_int(b,UVM_DEFAULT)
    `uvm_field_int(y,UVM_DEFAULT)
    `uvm_object_utils_end

endclass


// generator
// generator
/*
uvm_sequence #(type of data object, e.g., transaction)—> uvm_object

Register with the factory using uvm object utils

Declare transaction and any other local variables.

Constructor for the generator

Generate random transactions for DUT - use virtual task body and in it create a transaction object. Use loops as needed for generating multiple transactions. 
Use start_item() and finish_item() for sending data from sequencer to driver. 
start_item() —> we have a sequence to send
Generate sequence
finsih_item() —> we have sent the sequence to the driver. 

virtual task body();
t = transaction::type_id::create();
for(i=0;i<10;i++) begin
start_item(t);
t.randomize();
`uvm_info(“”GEN,$sformatf(”Data sent to Driver: a: %0d, b: %0d, mode: %0d”,a,b,mode),UVM_NONE);
t.print();
finish_item(t);
#10;
end
endtask

For combinational circuits, send the data first and then add delay for correct phasing.
*/

class generator extends uvm_sequence #(transaction);
    `uvm_object_utils(generator)

    transaction t;
    integer i;

    function new(input string inst ="GEN");
        super.new(inst);
    endfunction
    
    virtual task body();
    t = transaction::type_id::create("TRANS");
    for(i=0;i<10;i++) begin
    start_item(t);
        t.randomize();
        `uvm_info("GEN",$sformatf(”Data sent to Driver: a: %0d, b: %0d, mode: %0d”,a,b,mode),UVM_NONE);
        t.print(uvm_default_line_printer);
    finish_item(t);
    #10;
    end
    endtask
        
endclass

// driver
/*
uvm_driver #(type of data object) —> uvm_component

Get stimulus from generator and apply to the DUT using the interface
Access interface with config_db
get_next_item —> get next item from uvm_sequence
item_done —> request completed, ready to receive new request.
Apply data to DUT with virtual interface.

*/
class driver extends uvm_driver #(transaction);
    `uvm_component_utils(driver)

    transaction data;
    virtual au_if aif;

    function new(input string inst ="DRV", uvm_component c);
        super.new(inst,c);
    endfunction

    virtual function void build_phase(uvm_phase phase);
        super.build_phase(phase);
        t = transaction::type_id::create("TRANS");
        if(!uvm_config_db #(virtual au_if)::get(this,"","aif",aif))
            `uvm_info("DRV","Unable to read config DB",UVM_NONE);
    endfunction

    virtual task void run_phase(uvm_phase phase);
        forever begin
            seq_item_port.get_next_item(data);
            `uvm_info("DRV",$sformatf("Data sent to interface, a: %0d, b: %0d, mode: %0d",data.a,data.b,data.mode),UVM_NONE);
            aif.a=data.a;
            aif.b=data.b;
            aif.mode=data.mode;
            data.print(uvm_default_line_printer);
            seq_item_port.item_done();
        end
    endtask

endclass

//monitor
/*
uvm_monitor ---> uvm_component

Receive data from DUT using interface and send it to scoreboard
Update data container with data received from virtual interface
Use analysis port to send data to scoreboard
*/

class monitor extends uvm_monitor #(transaction);
    `uvm_component_utils(monitor)

    transaction t;
    virtual au_if aif;

    uvm_analysis_port #(transaction) send;

    function new(input string inst = "MON", uvm_component c);
        super.new(inst,c);
        send = new("WRITE",this);
    endfunction

    virtual function void build_phase(uvm_phase phase);
        super.build_phase(phase);
        t = transaction::type_id::create("MON");
        if(!uvm_config_db #(virtual au_if)::get(this,"","aif", aif)) 
            `uvm_info("MON",$sformatf("Data recived from DUT, a: %0d, b: %0d, mode: %0d, y: %0d",t.a,t.b,t.mode,t.y),UVM_NONE);
    endfunction

    virtual task run_phase(uvm_phase phase);
        forever begin 
        #10;
        t.a = aif.a;
        t.b = aif.b;
        t.mode = aif.mode;
        t.y = aif.y;
        `uvm_info("",$sformatf("Data send to ",t.a,t.b,t.mode,t.y),UVM_NONE);
        send.write(t);
        t.print(uvm_default_line_printer);
        end
    endtask



endclass

// agent
/*
uvm_agent —> uvm_component

Create instances of driver, sequencer, and monitor
Connect driver and sequencer

Driver —> initiator
Sequencer —> Target
*/
class agent extends uvm_agent;
    `uvm_component_utils(agent)

    driver d;
    monitor m;
    uvm_sequencer #(transaction) seq;

    
    function new(input string inst = "AGENT", uvm_component c);
        super.new(inst,c);
    endfunction

    virtual function void build_phase(uvm_phase phase);
        super.build_phase(phase);
        d = driver::type_id::create("DRV",this);
        m = monitor::type_id::create("MON",this);
        seq = uvm_sequencer #(transaction)::type_id::create("SEQ",this);
    endfunction

    virtual function void connect_phase(uvm_phase phase);
        super.connect_phase(phase);
        d.seq_item_port.connect(seq.seq_item_export);
    endfunction

endclass

// scoreboard
/*
uvm_scoreboard —> uvm_component

Receive data/transaction from monitor and compare with golden data
Use implementation of analysis port
*/
class scoreboard extends uvm_scoreboard #(transaction);
    `uvm_component_utils(scoreboard)

    transaction data;

    uvm_analysis_imp #(transaction,scoreboard) recv;

    function new(input string inst = "SCO", uvm_component c);
        super.new(inst,c);
        recv = new("Read",this);
    endfunction

    virtual function void build_phase(uvm_phase phase);
        super.build_phase(phase);
        t = trasaction::type_id::create("TRANS",this);
    endfunction

    virtual function void write(input transaction t);
        data = t;
        data.print(uvm_default_line_printer);
        //
        case(data.mode)
            2'b00: if(data.y == data.a + data.b) `uvm_info("SCO","Test PASSED",UVM_NONE); else `uvm_info("SCO","Test FAILED",UVM_NONE);
            2'b01: if(data.y == data.a - data.b) `uvm_info("SCO","Test PASSED",UVM_NONE); else `uvm_info("SCO","Test FAILED",UVM_NONE);
            2'b10: if(data.y == data.a * data.b) `uvm_info("SCO","Test PASSED",UVM_NONE); else `uvm_info("SCO","Test FAILED",UVM_NONE);
            2'b11: if(data.y == data.a / data.b) `uvm_info("SCO","Test PASSED",UVM_NONE); else `uvm_info("SCO","Test FAILED",UVM_NONE);
          endcase
    endfunction

endclass

// environment
/*
uvm_env —> uvm_component

Create instances of agent and scoreboard
Connect analysis port of monitor and scoreboard
Monitor —> Initiator
Scoreboard —> Target
*/
class env extends uvm_env;
    `uvm_component_utils(env)

    function new(input string inst="ENV", uvm_component c);
        super.new(inst,c);
    endfunction

    scoreboard s;
    agent a;
    
    virtual function void build_phase(uvm_phase phase);
        super.build_phase(phase);
        s = scoreboard::type_id::create("SCO",this);
        a = agent::type_id::create("AGENT", this);
    endfunction

    virtual function connect_phase(uvm_phase phase);
        super.connect_phase(phase);
        a.m.send.connect(s.recv);
    endfunction

endclass


// test
/*
uvm_test —> uvm_component

Create instances of environment
Connector generator and sequencer

Choose specific sequence to send to the driver
Generator —> Sequencer
*/
class test extends uvm_test;
    `uvm_component_utils(test)

    function new(input string inst = "TEST", uvm_component c);
        super.new(inst,c);
    endfunction

    generator g;
    env e;


    virtual function void build_phase(uvm_phase phase);
        super.build_phase(phase);
        g = generator::type_id::create("GEN",this);
        e = env::type_id::create("ENV",this);
    endfunction

    virtual task run_phase(uvm_phase phase);
        phase.raise_objection(phase);
        g.start(e.a.seq);
        phase.drop_objection(phase;)
    endtask

endclass


// testbench
/*
Create test instance
Create interface instance
Connect DUT to interface
Set virtual interface in uvm_config_db
*/
module tb;

    test t;
    au_if aif;
    au dut(.a(aif.a), .b(aif.b), .mode(aif.mode),.y(aif.y));

    initial begin
        $dumpvars;
        $dumpwaves("waves.vcd");
        uvm_config_db #(virtual au_if)::set(null,"*","aif",aif);
        t = new("TEST", null);
        run_test();
    end

endmodule

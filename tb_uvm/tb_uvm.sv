// UVM Testbench ; how events are schdule

// Driver
class driver extends uvm_driver;
// register class with factory
`uvm_component_utils("DRV","Driver",UVM_NONE)
// constructor
   function new(input string inst, uvm_component c);
      super.new(inst, c);
   endfunction // new
   
// build, connect and run phases
   virtual function void build_phase(uvm_phase phase);
      super.build_phase(phase);
      `uvm_info("DRV","Build Phase",UVM_NONE);
   endfunction // build_phase

   virtual function void connect_phase(uvm_phase phase);
      super.connect_phase(phase);
      `uvm_info("DRV","Connect Phase",UVM_NONE);
   endfunction // connect_phase

   virtual task run_phase(uvm_phase phase);
      `uvm_info("DRV","Run Phase",UVM_NONE);
   endtask // run_phase
   
   virtual function void report_phase(uvm_phase phase);
      super.report_phase(phase);




// Assignment 10
// Assume producer class consists of two data members (reg [7:0] a = 8'h12 and reg [15:0] b = 16'hffff). Use TLM blocking port to communicate this data to Consumer class. Also, print data member values in both Producer and Consumer class for verification.

// libraires and imports
`include "uvm_macros.svh"
import uvm_pkg::*;


// Producer
class producer extends uvm_component;
// register the class with factory
`uvm_component_utils(producer)
// data to send
   reg [7:0] a = 8'h12;
 /////  reg [15:0] b = 16'hffff;   

// create a uvm blocking port named "send"
   uvm_blocking_put_port #(int) send;
  
// constructor
   function new(input string inst, uvm_component c);
      super.new(inst,c);
      //`uvm_info("PROD","Producer",UVM_NONE);
      send = new("PUT",this);
   endfunction // new

// run task puts data
   task run();
     `uvm_info("PROD",$sformatf("Data A Sent to Consumer: %0d", a), UVM_NONE);
      send.put(a);
   endtask // run

// connect phase
   virtual function void connect_phase(uvm_phase phase);
      super.connect_phase(phase);
      p.send.connect(c.recv);
   endfunction // connect_phase

// end of elaboration phase - this shall print TB hierarchy from ENV
  virtual function void end_of_elaboration_phase(uvm_phase phase);
    super.end_of_elaboration_phase(phase);
    print();
  endfunction
   
endclass // env


// Test
class test extends uvm_component;
// register the class with the factory
   `uvm_component_utils(test)

// create an instance of environment   
   env e;

// constructor for test
   function new(input string inst, uvm_component c);
      super.new(inst,c);
   endfunction // new

// build phase 
   virtual function void build_phase(uvm_phase phase);
      super.build_phase(phase);
      e = env::type_id::create("ENV",this);
   endfunction // build_phase

// end of elaboration phase - this shall print the TB hierarchy from TEST
  virtual function void end_of_elaboration_phase(uvm_phase phase);
    super.end_of_elaboration_phase(phase);
    print();
  endfunction
 
// run phase
   virtual task run_phase(uvm_phase phase);
      #100;
      global_stop_request();    
   endtask // run
   
endclass // test


// TB

module tb();

   test t;

   initial begin
      t = new("TEST", null);
      run_test();
   end
   

endmodule // tb



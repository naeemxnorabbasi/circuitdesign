// include relevant libraries

`include "uvm_macros.svh"
import uvm_pkg::*;

// declare component A and B
class ComponentA extends uvm_component;
// register with the factory
   `uvm_component_utils(ComponentA)
   

// Data to send
   reg [7:0] din = 8'h11;
   

// create tlm blocking put port
  uvm_blocking_put_port #(reg [7:0]) send;
   
// constructor for Component A along with the tlm blocking put port
  function new(input string inst = "COMP_A", uvm_component c);
      super.new(inst,c);
      send = new("PUT",this);
   endfunction // new
   
// run task
   task run();
      `uvm_info("COMP_A",$sformatf("Data Sent to Component B: %0d",din),UVM_NONE);
     send.put(din);
   endtask // run
   
endclass // ComponentA

class ComponentB extends uvm_component;
// register with the factory
   `uvm_component_utils(ComponentB)
   
// create tlm blocking put port
  uvm_blocking_put_imp #(reg [7:0],ComponentB) recv;
   
// constructor for Component B along with the tlm blocking port implementation
   function new(input string inst = "COMP_B", uvm_component c);
      super.new(inst,c);
      recv = new("RECV",this);
   endfunction // new
   
// virtual task put
  virtual task put(input reg [7:0] t);
     `uvm_info("RECV",$sformatf("Data Received from Component A: %0d",t),UVM_NONE);   
   endtask // put
   
   
endclass // ComponentB


// Create environment
class env extends uvm_component;
// register class with the factory
   `uvm_component_utils(env)

// instantiate Components A and B
   ComponentA A;
   ComponentB B;

// constructors
   function new(input string inst, uvm_component c);
      super.new(inst,c);
   endfunction // new

// build phase
   virtual function void build_phase(uvm_phase phase);
      super.build_phase(phase);
      A = ComponentA::type_id::create("COMP_A",this);
      B = ComponentB::type_id::create("COMP_B",this);      
   endfunction // build_phase
   
// connect_phase
   virtual function void connect_phase(uvm_phase phase);
      super.connect_phase(phase);
      A.send.connect(B.recv);
   endfunction // connect_phase
   
endclass // env



// Create test
class test extends uvm_test;
// register class with the factory
   `uvm_component_utils(test);

// instantiate environment
   env e;
   
// constructor
   function new(input string inst, uvm_component c);
      super.new(inst,c);
   endfunction // new
   
// build phase
   virtual function void build_phase(uvm_phase phase);
      super.build_phase(phase);
      e = env::type_id::create("ENV",this);
   endfunction // build_phase

// end of elaboration
  virtual function void end_of_elaboration_phase(uvm_phase phase);
    super.end_of_elaboration_phase(phase);
    print();
  endfunction
  
// run phase
  virtual task run_phase(uvm_phase phase);
      #100;
      global_stop_request();
   endtask // run
    
endclass // test



// Create TB

module tb();

   test t;

   initial begin
      t = new("TEST_DUMMY", null);
      run_test();
   end
   

endmodule // tb



`include "uvm_macros.svh"
import uvm_pkg::*;


// interface
interface top_if();
logic [3:0] a;
logic [3:0] b;
logic [4:0] y;
endinterface


// design under test
module top (a,b,y);
input logic [3:0] a;
input logic [3:0] b;
output logic [4:0] y;

assign y = a + b;

endmodule


// transaction 
class transaction extends uvm_sequence_item;

  logic [3:0] a;
  logic [3:0] b;
  logic [4:0] y;
    
    function new(input string inst = "TRANS");
        super.new(inst);
    endfunction

  `uvm_object_utils_begin(transaction)
  `uvm_field_int(a,UVM_DEFAULT)
  `uvm_field_int(b,UVM_DEFAULT)
  `uvm_field_int(y,UVM_DEFAULT)
  `uvm_object_utils_end

endclass



// driver
class driver extends uvm_driver;
`uvm_component_utils(driver)

    virtual top_if vif;
    transaction t;

    function new (input string inst = "DRV", uvm_component c);
        super.new(inst,c);
    endfunction

    virtual function void build_phase(uvm_phase phase);
        super.build_phase(phase);
        t = transaction::type_id::create("TRANS",this);
      if(!uvm_config_db #(virtual top_if)::get(this,"","vif",vif)) 
            `uvm_info("DRV","Unable to read DB",UVM_NONE);
    endfunction

    virtual task run_phase(uvm_phase phase);
      phase.raise_objection(phase);
      t.a = 4'b0100;
      t.b = 4'b0011;
      t.print();
      vif.a = t.a;
      vif.b = t.b;
      phase.drop_objection(phase);
    endtask

endclass



// monitor
class monitor extends uvm_monitor;
    `uvm_component_utils(monitor)
    
        virtual top_if vif;
        transaction data;
    
        function new (input string inst = "MON", uvm_component c);
            super.new(inst,c);
        endfunction
    
        virtual function void build_phase(uvm_phase phase);
            super.build_phase(phase);
            data = transaction::type_id::create("TRANS",this);
          if(!uvm_config_db #(virtual top_if)::get(this, "", "vif", vif)) 
                `uvm_info("MON","Unable to read DB",UVM_NONE);
        endfunction
    
        virtual task run_phase(uvm_phase phase);
          phase.raise_objection(phase);
          #10;
          data.a = vif.a;
          data.b = vif.b;
          data.y = vif.y;
          data.print();
          phase.drop_objection(phase);
        endtask
    
    endclass

// env
class env extends uvm_env;
`uvm_component_utils(env)

    driver d;
    monitor m;

    function new (input string inst = "ENV", uvm_component c);
        super.new(inst,c);
    endfunction

    virtual function void build_phase(uvm_phase phase);
        super.build_phase(phase);
        d = driver::type_id::create("DRV",this);
        m = monitor::type_id::create("MON",this);
    endfunction

endclass


// test
class test extends uvm_test;
`uvm_component_utils(test)

    env e;

    function new(input string inst = "TEST", uvm_component c);
        super.new(inst,c);
    endfunction

    virtual function void build_phase(uvm_phase phase); 
        super.build_phase(phase);
      e = env::type_id::create("ENV",this);
    endfunction

endclass



// tb

module tb;

    test t;

    top_if vif();
    top dut(.a(vif.a),.b(vif.b), .y(vif.y));

    initial begin
      t = new("TEST", null);
      if(!uvm_config_db #(virtual top_if)::set(null,"*","vif",vif)) 
        `uvm_info("TEST","Unable to set DB",UVM_NONE);
      run_test();
    end


endmodule

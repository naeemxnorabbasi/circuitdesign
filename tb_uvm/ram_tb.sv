// design
module ram(
    input clk,wr,
    input [7:0] din,
    output reg [7:0] dout,
    input [3:0] addr
        );
    reg [7:0] mem [15:0];  
      
    always@(posedge clk)
    begin
    if(wr == 1'b1)
      mem[addr] <= din;
    else
      dout <= mem[addr];
    end  
endmodule

// interface
   ////////////////////////////////////////////////////////////
    interface ram_if();
        logic clk;
        rand logic wr;
        rand logic [7:0] din;
        rand logic [3:0] addr;
        logic [7:0] dout;
        endinterface
         
///////Test Bench///////
`include "uvm_macros.svh"
import uvm_pkg::*;

// transaction
class transaction extends uvm_sequence_item;
 
    logic wr;
    logic [7:0] din;
    logic [7:0] dout;
    logic [3:0] addr;

    function new(input string inst ="TRANS");
        super.new(inst);
    endfunction

    `uvm_object_utils_begin(transaction)
    `uvm_field_int(wr,UVM_DEFAULT)
    `uvm_field_int(din,UVM_DEFAULT)
    `uvm_field_int(dout,UVM_DEFAULT)
    `uvm_field_int(addr,UVM_DEFAULT)
    `uvm_object_utils_end()

endclass

// generator
class generator extends uvm_sequence #(transaction);
    `uvm_object_utils(generator)

    transaction t;
    integer i;

    function new(input string inst = "GEN");
        super.new(inst);
    endfunction

    virtual task body();
        t = transaction::type_id::create("TRANS");
        for(i=0; i<20; i++) begin
            start_item(t);
            t.randomize();
            t.print(uvm_default_line_printer);
            //`uvm_info(get_name(),$sformatf("Data sent to Driver: wr: %0d, din: %0d, addr: %0d",t.wr,t.din,t.addr),UVM_NONE);
            `uvm_info("GEN",$sformatf("Data sent to Driver: wr: %0d, din: %0d, addr: %0d",t.wr,t.din,t.addr),UVM_NONE);
            finish_item(t);
            @(posedge ram_if.clk);
        end
    endtask

endclass

// driver
class driver extends uvm_driver;
    `uvm_component_utils(driver)

    function new(input string inst = "DRV", uvm_component c);
        super.new(inst,c);
    endfunction

    transaction t;
    virtual ram_if rif;
    //uvm_sequencer #(transaction) seq;

    function new void build_phase(uvm_phase phase);
        super.build_phase(phase);
        t = transaction::type_id::create("TRANS");
        if(!uvm_config_db #(virtual ram_if)::get(this,"","rif",rif)) `uvm_info("DRV","Unable to read Config DB",UVM_NONE);
        //seq = uvm_sequencer #(transaction)::type_id::create("SEQ",this);
    endfunction

    virtual task run_phase(uvm_phase phase);
        forever begin
            seq_item_port.get_next_item(t);
            rif.wr = t.wr;
            rif.din = t.din;
            rif.addr = t.addr;
            //`uvm_info(get_name(),$sformatf("Data sent to DUT: wr: %0d, din: %0d, addr: %0d",rif.wr, rif.din,rif.addr),UVM_NONE);
            `uvm_info("DRV",$sformatf("Data sent to DUT: wr: %0d, din: %0d, addr: %0d",rif.wr, rif.din,rif.addr),UVM_NONE);
            t.print(uvm_default_line_printer);
            seq_item_port.item_done();
        end
    endtask



endclass

// monitor
class monitor extends uvm_monitor;
    `uvm_component_utils(monitor)

    transaction t;
    virtual ram_if rif;
    uvm_analysis_port #(transaction) send;

    function new(input string inst = "MON", uvm_component c);
        super.new(inst,c);
        send = new("Write",this);
    endfunction

    virtual function new void build_phase(uvm_phase phase);
        super.build_phase(phase);
        t = transaction::type_id::create("TRANS");
        if(!uvm_config_db #(virtual ram_if)::get(this,"","rif",rif)) `uvm_info("DRV","Unable to read Config DB",UVM_NONE);
    endfunction

    virtual task run_phase(uvm_phase phase);
        forever begin
            @(posedge clk);
            `uvm_inf("MON",$sformatf("Data read from RAM: wr: %0d, din: %0d, addr: %0d, dout: %0d",rif.wr,rif.din,rif,addr,rif,dout),UVM_NONE);
            t.wr = rif.wr;
            t.din = rif.din;
            t.addr = rif.addr;
            t.dout = rif.dout;
            send.write(t);
            t.print(uvm_default_line_printer);
        end
    endtask

endclass


// scoreboard
class scoreboard extends uvm_scoreboard;
    `uvm_component_utils(scoreboard)

    transaction t;
    uvm_analysis_imp #(transaction) recv;

    function new(input string inst = "SCO", uvm_component c);
        super.new(inst,c);
        recv = new("Read",this);
    endfunction

    virtual function new void build_phase(uvm_phase phase);
        super.build_phase(phase);
        t = transaction::type_id::create("TRANS");
    endfunction
    // checking mechanism not yet implemented

endclass

// agent
class agent extends uvm_agent;
    `uvm_component_utils(agent)

    function new(input string inst = "AGENT", uvm_component c);
        super.new(inst,c);
    endfunction

    driver d;
    monitor m;
    uvm_sequencer #(transaction) seq;
    
    virtual function void build_phase(uvm_phase phase);
        super.build_phase(phase);
        d = driver::type_id::create("DRV",this);
        m = monitor::type_id::create("MON",this);
        seq = uvm_sequencer #(transaction)::type_id::create("SEQ",this);                
    endfunction

    virtual function void connect_phase(uvm_phase phase);
        super.connect_phase(phase);
        d.seq_item_port.connect(seq.seq_item_export);
    endfunction

endclass

// env
class env extends uvm_env;
    `uvm_component_utils(env)

    function new(input string inst = "ENV", uvm_component c);
        super.new(inst,c);
    endfunction

    agent a;
    scoreboard s;

    virtual function void build_phase(uvm_phase phase);
        super.build_phase(phase);
        a = agent::type_id::create("AGENT",this);
        s = scoreboard::type_id::create("SCO",this);
    endfunction

    virtual function void connect_phase(uvm_phase phase);
        super.connect_phase(phase);
        a.m.send.connect(s.recv);
    endfunction

endclass

// test
class test extends uvm_test;
    `uvm_component_utils(test)

    function new(input string inst = "TEST", uvm_component c);
        super.new(inst,c);
    endfunction

    generator g;
    env e;

    virtual function void build_phase(uvm_phase phase);
        super.build_phase(phase);
        g = generator::type_id::create("GEN",this);
        e = env::type::create("ENV",this);
    endfunction

    virtual task run_phase(uvm_phase phase);
        phase.raise_objection(phase);
        g.start(e.a.seq);
        phase.drop_objection(phase);
    endtask

endclass

//tb
module tb;

    test t;
    ram_if rif();
    ram dut (.clk(rif.clk), .wr(rif.wr), .din(rif.din), .addr(rif.addr), .dout(rif.dout));

    initial begin
        rif.clk = 0;
    end

    always #10 rif.clk = ~rif.clk;

    initial begin
        $dumpvars;
        $dumpfile("waves.vcd");
        t = new("TEST",null);
        uvm_config_db #(virtual ram_if)::set(null,"*","rif",rif);
        uvm_top.enable_print_topology = 1;
        run_test();
    end


endmodule

input clk,wr,
input [7:0] din,
output reg [7:0] dout,
input [3:0] addr
    );
